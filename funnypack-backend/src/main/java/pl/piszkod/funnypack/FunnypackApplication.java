package pl.piszkod.funnypack;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunnypackApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunnypackApplication.class, args);
    }

}
