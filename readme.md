# Funnypack

## What is funnypack

Funnypack is a clone of Jackboxs' `Enough about you` from Jackbox Party Box 4.


## Version 0.0.1 Aims

* Run with any (modern-ish) browser
* Connect from any (modern-ish) phone (jackbox.tv style)
* Multiple language support (Polish as a first)

## Future development

* Voice-acted questions
* Option for users to easily add new languages
* Option for users to easily add new questions

### Dependencies

* Spring boot
  * It's something I have not used with Java for a long time
* [JIB](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin#quickstart)
  * Cause it's awesome and I wanted to try it for quite some time
* Docker compose
  * For easy local development
* Undertow instead of Tomcat
  * Allegedly Undertow has lower memory footprint and performance
